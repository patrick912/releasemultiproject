package fr.formation.ejb;

import javax.ejb.Local;

@Local
public interface IStateless {
	
	public int compteur();

}
